module Guru 
  module Api 
    module Middlewares 
      class JsonParser < Faraday::Response::Middleware
        def call(env)
          @app.call(env).on_complete do |environment|
            on_complete(environment)
          end
        end

        # Override this to modify the environment after the response has finished.
        # Calls the `parse` method if defined
        # `parse` method can be defined as private, public and protected
        def on_complete(env)
          return unless respond_to?(:parse, true) && env.parse_body? 

          env.body = parse(env.body)
        end
    
        private def parse(body)
          body= "{}" if body.empty?
          json = ::MultiJson.load(body, symbolize_keys: true)
          if json.is_a?(Hash) && json.try(:[], :errors)
           {
            data: {},
            metadata: {},
            errors: parse_errors(json[:errors])
           }
          else
           {
            data: json,
            metadata: {},
            errors: {}
           }
          end
        end

        private def parse_errors(errors)
          parsed_errors = {}  
          case errors 
          when Array 
            errors.each do |error|
              case error 
              when Hash 
                if error[:field] && (error[:defaultMessage] || error[:default_message])
                  parsed_errors[error[:field].downcase] ||= []
                  parsed_errors[error[:field].downcase] << {error: error[:defaultMessage] || error[:default_message], details: error}
                else
                  case error[error.keys.first]
                  when Array 
                    parsed_error[error.key.first.downcase] = error[error.key.first]
                  else 
                    parsed_error[error.key.first.downcase] ||= []
                    parsed_errors[error.keys.first.downcase] << {error: error[error.keys.first]} 
                  end 
                end 
              end 
            end
          when Hash 
            parsed_errors= errors 
          end  
          parsed_errors
        end 
      end
    end 
  end
end