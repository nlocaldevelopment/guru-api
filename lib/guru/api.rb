require "guru/api/version"
require "multi_json"

module Guru
  module Api
    class Error < StandardError; end
    # Your code goes here...
  end
end

Dir[File.expand_path("../api/**/*.rb", __FILE__)].each{ |f| require f}
